# Kubernetes - MQTT Mosquitto Broker

### Table of Contents
- [Expose Service Docs](https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/#exposing-the-service)

## Create a MQTT User and Password

Run docker mosquitto docker container to create a password for our passwd.yaml

```bash
docker run -it eclipse-mosquitto /bin/sh
```

Then run these commands from within the container

```bash
### Go to config folder
cd mosquitto/config

### Enter password
mosquitto_passwd -c passwordfile username
```

Copy the password from the output below and put in configmap.yaml file in "mosquitto-password" section

```bash
cat passwordfile

# Example Output: 
# username:$7$101$ADzYBT0JGsTlAoFU$6jr/H/oguXfRevZmX3VnD5xpJXE0G1MUlAOL0GZseThigt6gMjVWIkMefQDbCsg6Wg+sQbnovdjJQBQEo4AWjA==
```

## Apply Kubernetes Templates to cluster

Apply templates to kubernetes cluster from project root

```bash
kubectl -n $NAMESPACE apply -f .
```

## Connect to NodePort Server

Retrieve the NodePort and Node External IP

```bash
## Retrieve NodePort service details
kubectl -n $NAMESPACE get svc mosquitto -o yaml | grep nodePort -C 5

## Discover Node external ips
kubectl -n $NAMESPACE get nodes -o yaml | grep ExternalIP -C 1
```